# Running the project:

## Python venv (step not necessary but highly encouraged)

```bash
python3 -m venv ps_project
source ps_project/bin/activate
```

## Installing modules

```bash
sudo apt-get update
sudo apt-get install xclip
pip install -r requirements.txt
```

It is possible that something else might be needed. I didn't need those modules on Debian, but I did on Ubuntu.

```bash
pip install pygame pillow
```

Running the project should tell you if you miss something else.

## Running project

```bash
python client_gui.py
```
