import kivy
import os
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.uix.scrollview import ScrollView

from mcast_utils import CommunicationHandler

kivy.require("1.10.1")


class ScrollableLabel(ScrollView):
    """ Kivy does not provide a Scrollable Label out of the box so this is to create one """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # ScrollView does not allow to add more than one widget so we need to trick it
        # the widget added to the ScrollView will be a Layout and inside Layout we'll add
        # everything that is needed
        self.layout = GridLayout(cols=1, size_hint_y=None)
        self.add_widget(self.layout)

        self.chat_history = Label(size_hint_y=None, markup=True)

        # This is a "helper Label" used as an anchor to which the view will be scrolled
        # once a new message appears
        self.scroll_to_point = Label()

        self.layout.add_widget(self.chat_history)
        self.layout.add_widget(self.scroll_to_point)

    def update_chat_history(self, new_message):
        """ Method called whenever a new message appears to update chat history """

        self.chat_history.text += "\n" + new_message

        # Setting layout height to whatever the chat history height is and adding 15 pixels
        # so that there is a little bit of free space at the bottom
        # Setting chat history label to whatever height of chat history text is
        # Set width of chat history text to 98% of the label width (adds small margins)
        self.layout.height = self.chat_history.texture_size[1] + 15
        self.chat_history.height = self.chat_history.texture_size[1]
        self.chat_history.text_size = (self.chat_history.width * 0.98, None)

        # updating the heights above will make the whole thing bigger (duh)
        # ScrollView will add a scroll, but will not scroll to the bottom
        # that is why we need scroll_to_point
        self.scroll_to(self.scroll_to_point)


class ConnectPage(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # check if there are previous details and if so then
        # populate data with them else leave details empty
        if os.path.isfile("previous_details.txt"):
            with open("previous_details.txt", "r") as f:
                data = f.read().split(",")
                prev_ip = data[0]
                prev_port = data[1]
                prev_username = data[2]
                prev_interface_ip = data[3]
        else:
            prev_ip = ""
            prev_port = ""
            prev_username = ""
            prev_interface_ip = ""

        # define layout
        self.cols = 2

        # IP input field
        self.add_widget(Label(text="IP:"))
        self.ip = TextInput(text=prev_ip, multiline=False)
        self.add_widget(self.ip)

        # port input field
        self.add_widget(Label(text="Port:"))
        self.port = TextInput(text=prev_port, multiline=False)
        self.add_widget(self.port)

        # Username input field
        self.add_widget(Label(text="Username:"))
        self.username = TextInput(text=prev_username, multiline=False)
        self.add_widget(self.username)

        # Interface ip addr to use
        self.add_widget(
            Label(text="IP address of interface to use (leave empty for default):")
        )
        self.interface_ip = TextInput(text=prev_interface_ip, multiline=False)
        self.add_widget(self.interface_ip)

        # add join button!
        self.join = Button(text="Join")
        self.join.bind(on_press=self.join_button)
        self.add_widget(Label())  # place holder so that "Join" is on the right
        self.add_widget(self.join)

    def join_button(self, event):
        """ What happens after user clicks on "Join" """

        port = self.port.text
        ip = self.ip.text
        username = self.username.text
        iface_ip = self.interface_ip.text

        # Saving new details
        with open("previous_details.txt", "w") as f:
            f.write(f"{ip},{port},{username},{iface_ip}")

        info = f"Joining {ip}:{port} as {username} using {iface_ip}"
        message_app.info_page.update_info(info)
        message_app.screen_manager.current = "Info"

        # scheduling the connection itself
        Clock.schedule_once(self.connect, 1)

    def connect(self, _):
        # getting network info
        ip = self.ip.text
        port = self.port.text
        username = self.username.text
        iface_ip = self.interface_ip.text

        # Create chat window and show it
        message_app.create_chat_window()
        message_app.screen_manager.current = "Chat"


class InfoPage(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.cols = 1
        self.message = Label(halign="center", valign="middle", font_size=30)
        self.message.bind(width=self.update_text_width)
        self.add_widget(self.message)

    def update_info(self, new_message):
        self.message.text = new_message

    def update_text_width(self, *_):
        self.message.text_size = (self.message.width * 0.9, None)


class ChatWindow(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.cols = 1
        self.rows = 2

        self.history = ScrollableLabel(height=Window.size[1] * 0.9, size_hint_y=None)
        self.add_widget(self.history)

        self.new_message = TextInput(
            width=Window.size[0] * 0.8, size_hint_x=None, multiline=False
        )
        self.send = Button(text="Send")
        self.send.bind(on_press=self.send_message)

        bottom_line = GridLayout(cols=2)
        bottom_line.add_widget(self.new_message)
        bottom_line.add_widget(self.send)
        self.add_widget(bottom_line)

        # So we can send by clicking "Enter"
        Window.bind(on_key_down=self.on_key_down)

        self.connection_page = message_app.connect_page

        Clock.schedule_once(self.focus_text_input, 1)

    def get_communication_handler_reference(self, handler):
        self.communication_handler = handler

    def on_key_down(self, instance, keyboard, keycode, text, modifiers):
        """ Method that bind "Enter" key to sending message. May not work on every machine. """

        if keycode == 40:
            # That's "Enter"
            self.send_message(None)

    def focus_text_input(self, _):
        self.new_message.focus = True

    def send_message(self, _):
        # Grabbing a message to send
        to_send = self.new_message.text

        # Making TextInput empty so that user can type in new message
        self.new_message.text = ""

        if to_send:
            # if needed, because someone might have hit "Enter" or clicked on "Send"
            # Without writing anyting
            self.history.update_chat_history(
                f"[color=dd2020]{message_app.connect_page.username.text}[/color] > {to_send}"
            )
            self.communication_handler.send_buffer.append(to_send)

        # Focusing to the text field so user can continue to write
        Clock.schedule_once(self.focus_text_input, 0.1)

    def incoming_message(self, username, message):
        self.history.update_chat_history(f"[color=20dd20]{username}[/color] > {message}")


class MessageCastApp(App):
    def build(self):
        # using ScreenManager so we can change screens
        self.screen_manager = ScreenManager()

        # adding ConnectPage
        self.connect_page = ConnectPage()
        screen = Screen(name="Connect")
        screen.add_widget(self.connect_page)
        self.screen_manager.add_widget(screen)

        # adding InfoPage
        self.info_page = InfoPage()
        screen = Screen(name="Info")
        screen.add_widget(self.info_page)
        self.screen_manager.add_widget(screen)

        return self.screen_manager

    def on_stop(self):
        """ Method executed when user tries to exit the application. """

        self.communication_handler.finish_signal_read.set()

        # It is possible that read_thread will be in a state of recv() and if no message
        # arrives then read_thread will never finish. However knowing that it also receives
        # messages sent from the user running this app then we can send artificial message
        # so that read_thread will be able to check the state of finish_singal_read.
        # Additionally this let's other people know that a specific user quit the chat.

        self.communication_handler.send_buffer.append("QUITTING")
        self.communication_handler.read_thread.join()

        # Only once the read_thread is done (which is provided by .join() we finish the writing thread
        self.communication_handler.finish_signal_write.set()
        print("Closing")

    def create_chat_window(self):
        # Creating and adding ChatWindow to screen_manager
        self.chat_window = ChatWindow()
        screen = Screen(name="Chat")
        screen.add_widget(self.chat_window)
        self.screen_manager.add_widget(screen)

        self.communication_handler = CommunicationHandler(
            self.connect_page.ip.text,
            self.connect_page.port.text,
            self.connect_page.interface_ip.text,
        )

        self.chat_window.get_communication_handler_reference(self.communication_handler)
        self.communication_handler.initiate_chat(self.chat_window)


if __name__ == "__main__":
    message_app = MessageCastApp()
    message_app.run()
